module bitbucket.org/Sanny_Lebedev/test6

require (
	github.com/gorilla/mux v1.7.3
	github.com/jackc/pgx v3.5.0+incompatible
	github.com/rs/zerolog v1.15.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7
)
