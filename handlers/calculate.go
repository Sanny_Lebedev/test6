package handlers

import (
	"encoding/json"
	"math/big"
	"net/http"

	"bitbucket.org/Sanny_Lebedev/test6/fibb"
	"github.com/satori/go.uuid"
)

type (
	answer struct {
		UID     string `json:"UID"`
		Success bool   `json:"success"`
		Done    bool   `json:"done"`
		Meta    meta   `json:"meta"`
	}

	meta struct {
		Last    *big.Int   `json:"last"`
		Nums    []*big.Int `json:"nums"`
		Message string     `json:"msg,omitempty"`
	}
)

// home is a simple HTTP handler function which writes a response.
func calculate(w http.ResponseWriter, r *http.Request) {

	u1 := uuid.Must(uuid.NewV4()).String()

	w.Header().Set("Content-type", "application/json")

	bi := big.NewInt(0)

	// i, err := strconv.ParseInt(r.FormValue("n"), 10, 64)

	if _, ok := bi.SetString(r.FormValue("n"), 10); !ok {
		bi.SetInt64(0)
	}

	if bi.Cmp(big.NewInt(1)) == -1 {
		json.NewEncoder(w).Encode(answer{UID: u1, Success: false, Done: false, Meta: meta{Message: "Неверное начальное значение. Введите число больше 1"}})
	} else {
		json.NewEncoder(w).Encode(answer{UID: u1, Success: true, Done: false})
		go fibb.Calc(u1, bi)
	}

}
