package fibb

import (
	"math/big"
	"sync"
	"time"
)

var result, isDone sync.Map

// Calc - calculate
func Calc(jobID string, n *big.Int) {
	var i, j *big.Int
	res := make([]*big.Int, 0)
	for i, j = big.NewInt(0), big.NewInt(1); j.Cmp(n) == -1; i, j = big.NewInt(0).Add(i, j), i {
		time.Sleep(1000) // Тормозим обработку для наглядности
		res = append(res, i)
		result.Store(jobID, res)

	}
	isDone.Store(jobID, true)
}

func progressResult(jobID string) []*big.Int {
	res, ok := result.Load(jobID)
	if !ok {
		return nil
	}
	n, ok := res.([]*big.Int)
	if !ok {
		return nil
	}
	return n
}

func isCalcDone(jobID string) bool {
	res, ok := isDone.Load(jobID)
	if !ok {
		return false
	}
	done, ok := res.(bool)
	if !ok {
		return false
	}
	return done
}

// Status - get status of worker
func Status(jobID string) ([]*big.Int, bool) {
	return progressResult(jobID), isCalcDone(jobID)
}
