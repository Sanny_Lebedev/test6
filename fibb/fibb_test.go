package fibb

import (
	"math/big"
	"testing"
)

func TestFibb(t *testing.T) {

	res := []*big.Int{big.NewInt(0), big.NewInt(1), big.NewInt(1), big.NewInt(2), big.NewInt(3), big.NewInt(5), big.NewInt(8), big.NewInt(13)}

	Calc("1", big.NewInt(10))

	var val []*big.Int
	result := false
	wrong := false
	for result == false {
		val, result = Status("1")
	}

	for i, item := range val {
		if item.Cmp(res[i]) == 0 {
			wrong = true
		}
	}

	if !wrong {
		t.Errorf("Wrong result, \n Expected: %v \n Getted: %v", res, val)
	}

}
